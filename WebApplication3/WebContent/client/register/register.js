captureData = function(event) {
	var data = $('form[name="register"]').jsonify();
    console.log(data);
    $.ajax({
        type: "POST",
        url: "../../rest/register/v2",
        contentType: "application/json; charset=utf-8",
        crossDomain: true,
        dataType: "json",
        success: function(response) {
            if(response) {
                location.href = "../../index.html";
            }
            else {
                alert("No response");
            }
        },
        error: function(response) {
            alert("Error processing request: " + response.status);
            
        },
        data: JSON.stringify(data)
    }); 
    event.preventDefault();
};

window.onload = function() {
	var forms= $('form[name="register"]'); 
    forms[0].onsubmit = captureData;
}
