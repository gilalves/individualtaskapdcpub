var map;
var geocoder;
var markers = [];

function initMap() {
	map = new google.maps.Map(document.getElementById('map'), {
	    zoom: 9,
	    center: {lat: 38.736946, lng: -9.142685}
    });
    geocoder = new google.maps.Geocoder();
};

function geocodeAddress(content) {
	geocoder.geocode({'address': content.street + " " + content.city + " " + content.postCode}, 
			function(results, status) {
      if (status === 'OK') {
    	  map.setCenter(results[0].geometry.location);
    	  addMarker(results[0].geometry.location, content);    	  
      } else {
        alert('Geocode was not successful for the following reason: ' + status);
      }
   });
};


function clearMap() {
	for (var i = 0; i < markers.length; i++) {
	    markers[i].setMap(null);
	 }
}


function addMarker(location, content) {
    var marker = new google.maps.Marker({
      position: location,
      map: map,
    });
    var contentString = '<p>Email: <u>' + content.email + '</u></p>' +
						'<p>Address: <u>' + content.street + ', ' + content.postCode + ' ' + content.city + '</u></p>' +
						'<p>Mobile: <u>' + content.mobile + '</u>		Landline: <u>' + content.landLine + '</u></p>' +
						'<p>CC nr: <u>' + content.cc + '</u>	NIF: <u>' + content.nif + '</u></p>';
    var infowindow = new google.maps.InfoWindow({content: contentString});	
    marker.addListener('click', function() {
        infowindow.open(map, marker);
    });
    markers.push(marker);
};

  
function getUsername() {
	var username = localStorage.getItem("username");
	return username;
}

searchUser = function(event) {
	clearMap();
	var data = $('#searchUser').val();
	console.log("DATA: " + data);
	$.ajax({
		type: "GET",
		url: "/rest/maps/" + data,
		contentType: "application/json; charset=utf-8",
		crossDomain: true,
		success: function(response) {
			console.log(response.propertyMap);
			geocodeAddress(response);
		},
		error: function(response) {
			$('input[id="searchUser"]').on('keydown', function(){
       		  var error = document.getElementById("userNotFound");
       		  if(error.innerHTML != "") {
       			  error.innerHTML="";
       		  }
       	  });
       	  var error = document.getElementById("userNotFound");
         	  error.innerHTML = "User not found.";
		}
		
	});
	event.preventDefault();
};


getAllAddresses = function(event) {
	clearMap();
	$.ajax({
		type: "GET",
		url: "/rest/maps/all",
		contentType: "application/json; charset=utf-8",
		crossDomain: true,
		success: function(response) {
			for(var i=0; i< response.length; i++) {
				console.log(response[i].email)
				geocodeAddress(response[i]);
			}
		},
		error: function(response) {
			alert("deu mal");
		}
	});
	event.preventDefault();
};


//removeToken = function(event) {
//	
//	var username = localStorage.getItem("username");
//	var token = localStorage.getItem("tokenID");
//	
//	var data = {username};
//	$.ajax({
//		type: "POST",
//		url: "/rest/logout/",
//        contentType: "application/json; charset=utf-8",
//        crossDomain: true,
//        dataType: "json",
//        success: function(response) {
//        	if(response) {
//        		localStorage.clear();
//        		location.href="index.html";
//        	}
//        	else alert("No response");
//        	
//        },
//        error: function(response) {
//        	alert("LOOK: " + response)
//        },
//        data: JSON.stringify(data)
//	});
//	event.preventDefault();
//};


removeToken = function() {
	localStorage.clear();
	location.href="index.html";
};


window.onload = function() {
	//testing if token is in local storage
	var forms= $('form[name="searchForm"]'); 
    forms[0].onsubmit = searchUser;
    
    var forms2= $('form[name="showAll"]'); 
    forms2[0].onsubmit = getAllAddresses;
	
	var logout = document.getElementById("logout");
	logout.onclick = removeToken;
	
	var username = localStorage.getItem("username").split("@");
	var id = document.getElementById("username");
	id.innerHTML = username[0];
}