function handleLogin(event, identifier, password) {
	var array = {identifier, password};
	var data = JSON.stringify(array);
	console.log(data);
	 $.ajax({
        type: "POST",
        url: "/rest/login",
        contentType: "application/json; charset=utf-8",
        crossDomain: true,
        dataType: "json",
        success: function(response) {
            if(response) {
            	localStorage.setItem('username', response.username);
            	localStorage.setItem('tokenID', response.tokenID);
            	location.href = "test.html";
            }
            else {
                alert("Incorrect Password");
            }
        },
        error: function(response) {
        	  $('input[id="searchUser"]').on('keydown', function(){
        		  var error = document.getElementById("passwordError");
        		  if(error.innerHTML != "") {
        			  error.innerHTML="";
        		  }
        	  });
        	  var error = document.getElementById("passwordError");
          	  error.innerHTML = "You entered a wrong password.";
        	
        },
        data: JSON.stringify(array)
        
    }); 
    event.preventDefault();
};


captureData = function(event) {
	var data = $('form[name="login"]').jsonify();
	console.log(data);
    $.ajax({
        type: "GET",
        url: "/rest/login/" + document.getElementById("ident").value,
        contentType: "application/json; charset=utf-8",
        crossDomain: true,
        dataType: "json",
        success: function(response) {
            if(response) {
            	handleLogin(event, response.name, data.password); 
            }
            else {
            	//user doesn't exist 
            }
        },
        error: function(response) {
//            alert("Error processing request: " + response.status);
        	$('input[id="ident"]').on('keyup', function() {
        		var error = document.getElementById("usernameError");
        		if(error.innerHTML!="") {
        			error.innerHTML="";
        		}
        			
        	});
        	var error = document.getElementById("usernameError");
        	error.innerHTML = "Username doesn't exist.";
            
            
        },
        data: JSON.stringify(data)
    }); 
    event.preventDefault();
};

window.onload = function() {
	var token = localStorage.getItem("tokenID");
	if(token != null) {
		location.href="test.html";
	}
	else {
		var forms= $('form[name="login"]'); 
	    forms[0].onsubmit = captureData;
	}
	
	
    
}



