function handleLogin(event, identifier, password) {
	var array = {identifier, password};
	var data = JSON.stringify(array);
	console.log(data);
	 $.ajax({
        type: "POST",
        url: "/rest/login",
        contentType: "application/json; charset=utf-8",
        crossDomain: true,
        dataType: "json",
        success: function(response) {
            if(response) {
            	localStorage.setItem('tokenID', response);
            	alert("tokenID: " + response);
            }
            else {
            	//password does not match
                alert("Incorrect Password");
            }
        },
        error: function(response) {
            alert("Error processing request: " + response.status); 
        },
        data: JSON.stringify(array)
        
    }); 
    event.preventDefault();
};


captureData = function(event) {
	var data = $('form[name="login"]').jsonify();
	console.log(data);
    $.ajax({
        type: "GET",
        url: "/rest/login/" + document.getElementById("ident").value,
        contentType: "application/json; charset=utf-8",
        crossDomain: true,
        dataType: "json",
        success: function(response) {
            if(response) {
            	alert(response.name + "  " + data.password)
            	handleLogin(event, response.name, data.password); 
            }
            else {
            	//user doesn't exist 
                alert("User does not exist");
            }
        },
        error: function(response) {
            alert("Error processing request: " + response.status); 
        },
        data: JSON.stringify(data)
    }); 
    event.preventDefault();
};

window.onload = function() {
    var forms= $('form[name="login"]'); 
    forms[0].onsubmit = captureData;
}
