package pt.unl.fct.di.apdc.firstwebapp.resources;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import pt.unl.fct.di.apdc.firstwebapp.util.InfoDisplay;

import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.EmbeddedEntity;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.EntityNotFoundException;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.PreparedQuery;
import com.google.appengine.api.datastore.Query;
import com.google.gson.Gson;


@Path("/maps")
@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
public class OperationResources {
	
	private static final DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
	private final Gson g = new Gson();
	
	public OperationResources() {}
	
	
	@GET
	@Path("/all")
	public Response getAllAddress() {
		List<InfoDisplay> list = new ArrayList<InfoDisplay>();
		Query query = new Query("User");
		PreparedQuery pq = datastore.prepare(query);
		for (Entity result : pq.asIterable()) {
			InfoDisplay info = generateFields(result);
			list.add(info);
		}
		if(!list.isEmpty()) {
			return Response.ok(g.toJson(list)).build();
		}
		else {
			return Response.status(Status.FORBIDDEN).entity("No users registered.").build();
		}
	}
	
	@GET
	@Path("/{identifier}")
	public Response getAddress( @PathParam("identifier") String identifier) {
		Key userKey = KeyFactory.createKey("User", identifier);
		try {
			Entity user = datastore.get(userKey);
			InfoDisplay info = generateFields(user);
			return Response.ok(g.toJson(info)).build();
		}catch(EntityNotFoundException e) {
			return Response.status(Status.FORBIDDEN).entity("User doesn't exist.").build();
		}
	}
	
	private InfoDisplay generateFields(Entity user) {
		EmbeddedEntity address = (EmbeddedEntity) user.getProperty("address");
		String email = (String)user.getKey().toString();
		email = email.substring(6, email.length()-2);
		String nif = (String)user.getProperty("nif");
		String cc = (String)user.getProperty("cc");
		String street = (String)address.getProperty("street");
		String comp= (String)address.getProperty("comp");
		String postCode = (String)address.getProperty("postCode"); 
		String city = (String)address.getProperty("city");
		String mobile = (String)user.getProperty("mobile");
		String landLine = (String)user.getProperty("phone");
		return new InfoDisplay(email, nif, cc, street, comp, postCode, city, mobile, landLine);
	}
}
