package pt.unl.fct.di.apdc.firstwebapp;

import java.io.IOException;
import javax.servlet.http.*;

@SuppressWarnings("serial")
public class WebApplicationServlet extends HttpServlet {
	public void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {
		resp.setContentType("text/plain");
		resp.getWriter().println("Hello, world");
	}
}
