package pt.unl.fct.di.apdc.firstwebapp.resources;

import javax.ws.rs.Consumes;




import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.EntityNotFoundException;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.PreparedQuery;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.Query.CompositeFilterOperator;

import com.google.appengine.api.datastore.Query.Filter;
import com.google.appengine.api.datastore.Query.FilterPredicate;
import com.google.appengine.api.datastore.Query.FilterOperator;
import com.google.appengine.api.datastore.Query.CompositeFilter;
import com.google.gson.Gson;

import pt.unl.fct.di.apdc.firstwebapp.util.AuthToken;
import pt.unl.fct.di.apdc.firstwebapp.util.LoginData;

import org.apache.commons.codec.digest.DigestUtils;

@Path("/login")
@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
public class LoginResources {
	
	private static final DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
	private final Gson g = new Gson();
	
	public LoginResources() {
		
	}
	
	@POST
	@Path("/")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response login(LoginData data) {
		Key userKey = KeyFactory.createKey("User", data.identifier);
		Filter keyFilter = new FilterPredicate(Entity.KEY_RESERVED_PROPERTY, FilterOperator.EQUAL, userKey);
		Filter passFilter = new FilterPredicate("password", FilterOperator.EQUAL, DigestUtils.sha512Hex(data.password));
		CompositeFilter filter = CompositeFilterOperator.and(keyFilter, passFilter);
		
		Query query = new Query("User").setFilter(filter);
		PreparedQuery pq = datastore.prepare(query);
		Entity user = pq.asSingleEntity();
		if(user!=null) {
			//generate token
			AuthToken token = generateToken(user, data.identifier);
			return Response.ok(g.toJson(token)).build();
		}
		return Response.status(Status.FORBIDDEN)
				   .entity(g.toJson("Incorrect password")).build();
	
	}
	
	@GET
	@Path("/{identifier}")
	public Response existingUser( @PathParam("identifier") String identifier) {
		try {
			Key userKey = KeyFactory.createKey("User", identifier);
			Entity user = datastore.get(userKey);
			return Response.ok().entity(g.toJson(user.getKey())).build();
		}
		catch(EntityNotFoundException e) {
			return testFields(identifier);
		}
	}
	

	public Response testFields(String identifier) {
		Filter nifFilter = new FilterPredicate("nif", FilterOperator.EQUAL, identifier);
		Filter ccFilter = new FilterPredicate("cc", FilterOperator.EQUAL, identifier);
		
		CompositeFilter filter = CompositeFilterOperator.or(nifFilter, ccFilter);
		Query query = new Query("User").setFilter(filter);
		PreparedQuery pq = datastore.prepare(query);
		
		Entity user = pq.asSingleEntity();
		
		if(user!=null) {
			Key userKey = user.getKey();
			return Response.ok().entity(g.toJson(userKey)).build();
		}
		return Response.status(Status.FORBIDDEN)
				   .entity(g.toJson("Username doesn't exist.")).build();
		
	}
	
	private AuthToken generateToken(Entity user, String username) {
		AuthToken auth = new AuthToken(username);
		Entity token =  new Entity("Auth", auth.getTokenId());
		
		token.setProperty("creationDate", auth.getCreationData());
		token.setProperty("expirationDate", auth.getExpirationData());
		token.setProperty("username", auth.getUsername());
		datastore.put(token);
		
		return auth;
	}
}
