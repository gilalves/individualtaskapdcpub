package pt.unl.fct.di.apdc.firstwebapp.resources;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.PreparedQuery;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Query.FilterOperator;
import com.google.appengine.api.datastore.Query.Filter;
import com.google.appengine.api.datastore.Query.FilterPredicate;
import com.google.gson.Gson;

import pt.unl.fct.di.apdc.firstwebapp.util.LogoutData;

@Path("/logout")
@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
public class LogoutResources {
	
	private static final DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
	private final Gson g = new Gson();
	
	
	public LogoutResources() {}
	
	@POST
	@Path("/")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response doLogout(LogoutData data) {
		Key userKey = KeyFactory.createKey("Auth", data.username);
		
		String key = userKey.toString();
		
		Filter filter = new FilterPredicate("username", FilterOperator.EQUAL, data.username);
		Query query = new Query("Auth").setFilter(filter);
		PreparedQuery pq = datastore.prepare(query);
		Entity auth = pq.asSingleEntity();
		if(auth!=null) {
			datastore.delete(auth.getKey());
			return Response.ok(g.toJson("The username is" + data.username)).build();
		}
		else {
			return Response.ok(g.toJson(key)).build();
		}
	}
}
