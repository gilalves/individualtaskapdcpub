package pt.unl.fct.di.apdc.firstwebapp.util;

public class InfoDisplay {
	
	public String email;
	public String nif;
	public String cc;
	public String street;
	public String comp;
	public String postCode;
	public String city;
	public String mobile;
	public String landLine;
	public InfoDisplay(String email, String nif, String cc, String street,
			String comp, String postCode, String city, String mobile,
			String landLine) {
		this.email = email;
		this.nif = nif;
		this.cc = cc;
		this.street = street;
		this.comp = comp;
		this.postCode = postCode;
		this.city = city;
		this.mobile = mobile;
		this.landLine = landLine;
	}
	
	
	
	

}
