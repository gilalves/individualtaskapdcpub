package pt.unl.fct.di.apdc.firstwebapp.util;

public class RegisterData {
	
	public String email;
	public String phone;
	public String mobile;
	public String street;
	public String comp;
	public String city;
	public String postCode;
	public String nif;
	public String ccNumber;
	public String password;
	
	public RegisterData() {
		
	}

	public RegisterData(String email, String phone, String mobile, 
			String street, String comp, String city, String postCode, 
				String nif, String ccNumber, String password) {
		this.email = email;
		this.phone = phone;
		this.mobile = mobile;
		this.street = street;
		this.comp = comp;
		this.city = city;
		this.postCode = postCode;
		this.nif = nif;
		this.ccNumber = ccNumber;
		this.password = password;
	}
	
	
	
	

}
