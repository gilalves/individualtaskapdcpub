package pt.unl.fct.di.apdc.firstwebapp.resources;

import javax.ws.rs.Consumes;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import pt.unl.fct.di.apdc.firstwebapp.util.RegisterData;

import com.google.appengine.api.datastore.EmbeddedEntity;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.EntityNotFoundException;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Transaction;
import com.google.gson.Gson;
import org.apache.commons.codec.digest.DigestUtils;


@Path("/register")
@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
public class RegisterResources {
	
	private static final DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
	private final Gson g = new Gson();
	
	public RegisterResources() {}
	
	@POST
	@Path("/v2")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response register2(RegisterData data) {
		Transaction txn = datastore.beginTransaction();
		try {
			Key userKey = KeyFactory.createKey("User", data.email);
			
			@SuppressWarnings("unused")
			Entity user = datastore.get(userKey);
			txn.rollback();
			return Response.status(Status.BAD_REQUEST).entity("User already exists.").build();
		}catch(EntityNotFoundException e) {
			
			Entity user = new Entity("User", data.email);
			
			user.setProperty("phone", data.phone);
			user.setProperty("mobile", data.mobile);
			user.setProperty("address", 
					buildAddress(data.street, data.comp, data.city, data.postCode));
			user.setProperty("nif", data.nif);
			user.setProperty("cc", data.ccNumber);
			user.setProperty("password", DigestUtils.sha512Hex(data.password));
			
	
			datastore.put(txn, user);
			txn.commit();
			
			
			return Response.ok(g.toJson("User added.")).build();
		}
		finally {
			if(txn.isActive()) {
				txn.rollback();
			}
		}
	}
	
	private EmbeddedEntity buildAddress(String street, String comp, String city, String postCode) {
		EmbeddedEntity address = new EmbeddedEntity();
		address.setProperty("street", street);
		address.setProperty("comp", comp);
		address.setProperty("city", city);
		address.setProperty("postCode", postCode);
		return address;
	}	
		
}
